import axios from "axios";

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const fetchContactsSuccess = payload => ({type: FETCH_CONTACTS_REQUEST, payload})
export const fetchContacts = () => {
    return async dispatch => {
        try {
            const contactsResponse = await axios.get('https://exam-9-contacts-kani-default-rtdb.firebaseio.com/contacts.json');
            const contactsArr = Object.keys(contactsResponse.data).map(contact => (
                {...contactsResponse.data[contact], id: contact}
            ));
            dispatch(fetchContactsSuccess(contactsArr));
        } catch (e) {

        }
    }
}

export const FETCH_SINGLE_CONTACT_REQUEST = 'FETCH_SINGLE_CONTACT_REQUEST';
export const fetchSingleContactSuccess = payload => ({type:FETCH_SINGLE_CONTACT_REQUEST, payload});
export const fetchSingleContact = (id) => {
    return async dispatch => {
        try {
            const singleContactResponse = await axios.get(`https://exam-9-contacts-kani-default-rtdb.firebaseio.com/contacts/${id}.json`);
            dispatch(fetchSingleContactSuccess(singleContactResponse.data));
        } catch (e) {

        }
    }
}

export const updateContact = (id, contact, history) => {
    return async dispatch => {
        try {
            await axios.put(`https://exam-9-contacts-kani-default-rtdb.firebaseio.com/contacts/${id}.json`, contact);
            history.push('/');
        } catch (e) {

        }
    }
}

export const RESET_CONTACT_TO_UPDATE = 'RESET_CONTACT_TO_UPDATE';
export const resetContactToUpdate = () => ({type: RESET_CONTACT_TO_UPDATE});

export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const deleteContactSuccess = payload => ({type: DELETE_CONTACT_REQUEST, payload});
export const deleteContact = (id, history) => {
    return async (dispatch) => {
        try {
            await axios.delete(`https://exam-9-contacts-kani-default-rtdb.firebaseio.com/contacts/${id}.json`);
            dispatch(deleteContactSuccess(id));
        } catch (e) {

        }
    }
}

export const createContact = (contact) => {
    return async dispatch => {
        try {
            axios.post('https://exam-9-contacts-kani-default-rtdb.firebaseio.com/contacts.json', contact);
        } catch (e) {

        }
    }
}