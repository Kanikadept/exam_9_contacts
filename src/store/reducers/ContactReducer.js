import {
    DELETE_CONTACT_REQUEST,
    FETCH_CONTACTS_REQUEST,
    FETCH_SINGLE_CONTACT_REQUEST,
    RESET_CONTACT_TO_UPDATE
} from "../actions/ContactActions";

const initialState = {
    contacts: [],
    contactToUpdate: null,
}

const contactReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {...state, contacts: action.payload}
        case FETCH_SINGLE_CONTACT_REQUEST:
            return {...state, contactToUpdate: action.payload}
        case DELETE_CONTACT_REQUEST:
            const contactsCopy = [...state.contacts];
            const removeIndex = contactsCopy.map(contact => {
                return contact.id;
            }).indexOf(action.payload);
            contactsCopy.splice(removeIndex, 1)
            return {...state, contacts: contactsCopy}
        case RESET_CONTACT_TO_UPDATE:
            return {...state, contactToUpdate: null}
        default:
            return  state;
    }
}

export default contactReducer;