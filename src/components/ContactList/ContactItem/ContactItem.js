import React, {useState} from 'react';
import './ContactItem.css';
import Modal from "../../UI/Modal/Modal";
import {NavLink, useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteContact} from "../../../store/actions/ContactActions";


const ContactItem = ({id, image, name, email, phone}) => {

    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState(false);
    const history = useHistory();


    const handleCloseModal = () => {
        setShowModal(false);
    }
    const handleShowModal = () => {
        setShowModal(true);
    }

    const handleDelete = () => {
        dispatch(deleteContact(id, history));
    }

    return (
        <>
            <div className="contact-item" onClick={handleShowModal}>
                <div className="image"><img src={image} alt=""/></div>
                <span>{name}</span>
            </div>
            <Modal show={showModal} closed={handleCloseModal}>
               <div>
                   <div className="contact-info">
                       <div className="image"><img src={image} alt=""/></div>
                       <div className="contact-info-right">
                           <span className="name">{name}</span>
                           <span className="phone">{phone}</span>
                           <span className="email">{email}</span>
                       </div>
                   </div>
                   <div className="contacts-info-btns">
                       <NavLink to={`/form/${id}`}><button>Edit</button></NavLink>
                       <button onClick={handleDelete}>Delete</button>
                   </div>
               </div>
            </Modal>
        </>

    );
};

export default ContactItem;