import React, {useEffect} from 'react';
import './ContactList.css';
import ContactItem from "./ContactItem/ContactItem";
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts} from "../../store/actions/ContactActions";

const ContactList = () => {

    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts);

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch])

    return (
        <>
            <div className="contact-list">

                {contacts.map(contact => {
                    return <ContactItem key={contact.id}
                                        id={contact.id}
                                        name={contact.name}
                                        image={contact.photo}
                                        phone={contact.phone}
                                        email={contact.email}/>
                })}
            </div>
        </>
    );
};

export default ContactList;