import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {resetContactToUpdate} from "../../../store/actions/ContactActions";

const Header = () => {

    const dispatch = useDispatch();

    const handleAddContact = () => {
        dispatch(resetContactToUpdate());
    }

    return (
        <header className="header">
            <span>Contacts</span>
            <NavLink to="/form"><button onClick={handleAddContact}>Add new contact</button></NavLink>

        </header>
    );
};

export default Header;