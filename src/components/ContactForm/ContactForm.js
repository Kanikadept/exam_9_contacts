import React, {useState, useEffect} from 'react';
import './ContactForm.css';
import {useDispatch, useSelector} from "react-redux";
import {createContact, fetchContacts, fetchSingleContact, updateContact} from "../../store/actions/ContactActions";
import {NavLink} from "react-router-dom";

const ContactForm = props => {

    const dispatch = useDispatch();
    const contactToUpdate = useSelector(state => state.contactToUpdate);

    const [contact, setContact] = useState({
        'name': '',
        'phone': '',
        'email': '',
        'photo': '',
    });

    useEffect(() => {
        if(props.match.params.id) {
            dispatch(fetchSingleContact(props.match.params.id));
        }

    }, [dispatch, props.match.params.id])

    useEffect(() => {
        if(contactToUpdate) {
            setContact(contactToUpdate);
        }
    }, [contactToUpdate])

    const handleChange = (event) => {
        const {name, value} = event.target;
        const contactCopy = {...contact};
        contactCopy[name] = value;
        setContact(contactCopy);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        if (contactToUpdate) {
            dispatch(updateContact(props.match.params.id, contact, props.history));
            dispatch(fetchContacts());
            setContact({
                'name': '',
                'phone': '',
                'email': '',
                'photo': '',
            });
        } else {
            dispatch(createContact(contact));
            setContact({
                'name': '',
                'phone': '',
                'email': '',
                'photo': '',
            });
        }
    }

    return (
        <div className="contact-form">
            <div><span>Add new contact</span></div>
            <form onSubmit={handleSubmit}>
                <div className="form-row">
                    <label>Name</label><input type="text" name="name"
                                              onChange={handleChange} value={contact.name || ''} required/>
                </div>
                <div className="form-row">
                    <label>Phone</label><input type="text" name="phone"
                                               onChange={handleChange} value={contact.phone || ''} required/>
                </div>
                <div className="form-row">
                    <label>Email</label><input type="email" name="email"
                                               onChange={handleChange} value={contact.email || ''} required/>
                </div>
                <div className="form-row">
                    <label>Photo</label><input type="text" name="photo"
                                               onChange={handleChange} value={contact.photo || ''}/>
                </div>
                <div className="form-row">
                    <label>Photo preview</label><div className="preview"><img src={contact.photo} alt=""/></div>
                </div>
                <button className="contact-save">Save</button>
                <NavLink to="/"><button className="contact-back">Back to contacts</button></NavLink>
            </form>
        </div>
    );
};

export default ContactForm;