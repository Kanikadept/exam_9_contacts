import './App.css';
import {Switch, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import ContactList from "./components/ContactList/ContactList";
import ContactForm from "./components/ContactForm/ContactForm";

const App = () => (
    <div className="App">
       <div className="container">
           <Layout>
               <Switch>
                   <Route path="/" exact component={ContactList}/>
                   <Route path="/form" exact component={ContactForm}/>
                   <Route path="/form/:id" exact component={ContactForm}/>
               </Switch>
           </Layout>
       </div>
    </div>
);

export default App;
